#!/bin/sh
#
# Start the emulation in a detachable screen session.
# Escape key is control-p instead of the default control-e,
# leaving control-e available for interrupting simh.

screen -e^Pp -h 2048 -S IS_VAXBINDEC07 infoserver150vxt IS_VAXBINDEC07.ini 
